import Container = PIXI.Container;

export class UIContainer extends Container {
    anchorX = "t";
    anchorY = "l";
    yPos = 0;
    xPos = 0;
    w = 0;
    h = 0;
    useParentSize = true;
    enabledAutoSetPos = true;

    updatePosition(parentW: number, parentH: number) {
        if (this.enabledAutoSetPos) {
            let x = 0;
            let y = 0;

            if (this.useParentSize) {
                this.w = parentW;
                this.h = parentH;
            } else {
                this.w = this.width;
                this.h = this.height;
            }

            switch (this.anchorX) {
                case "c":
                    x = this.xPos + (parentW - this.w) * 0.5;
                    break;
                case "l":
                    x = this.xPos;
                    break;
                case "r":
                    x = parentW - this.w - this.xPos;
                    break;
            }
            switch (this.anchorY) {
                case "c":
                    y = this.yPos + (parentH - this.h) * 0.5;
                    break;
                case "t":
                    y = this.yPos;
                    break;
                case "b":
                    y = parentH - this.h - this.yPos;
                    break;
            }

            this.x = x;
            this.y = y;

            for (let i = 0; i < this.children.length; i++) {
                let child = this.children[i];
                if (child instanceof UIContainer) {
                    child.updatePosition(this.w, this.h);
                }
            }
        }
    }
}