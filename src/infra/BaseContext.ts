import Application = PIXI.Application;
import Graphics = PIXI.Graphics;
import {UIContainer} from "./UIContainer";

export class BaseContext {
    private sizeRangeX: Array<number>;
    private sizeRangeY: Array<number>;
    private parentHTML: HTMLElement;

    public back: Graphics = new Graphics();

    public app: Application;
    public contentContainer: UIContainer;

    init(parentHTML: HTMLElement, sizeX: Array<number>, sizeY: Array<number>, bgColor: number = 0x222222) {
        this.sizeRangeX = sizeX;
        this.sizeRangeY = sizeY;
        this.parentHTML = parentHTML;
        this.app = new Application(sizeX[0], sizeY[0], {backgroundColor: bgColor});
        this.contentContainer = new UIContainer();
        this.back.renderable = false;
        this.app.stage.addChild(this.contentContainer);
        this.contentContainer.addChild(this.back);
        this.parentHTML.appendChild(this.app.view);
        $(this.app.renderer.view).css("height", "100%");
        $(this.app.renderer.view).css("width", "100%");
        window.addEventListener("resize", () => {
            this.resize();
        });
        this.app.ticker.add((dt: number) => {
            this.contentContainer.updatePosition( this.app.renderer.width,  this.app.renderer.height);
        });
    }

    resize() {
        let minScreenW = this.sizeRangeX[0];
        let maxScreenW = this.sizeRangeX[1];
        let minScreenH = this.sizeRangeY[0];
        let maxScreenH = this.sizeRangeY[1];

        if (this.app.renderer) {
            let iW = $(window).width();
            let iH = $(window).height();

            let iW_ = iW;
            let iH_ = iH;
            let sX = 0;
            let sY = 0;

            if (iW_ < minScreenW || iH_ < minScreenH) {
                if (iW_ / minScreenW < iH_ / minScreenH) {
                    iW = minScreenW;
                    iH = iH_ * (minScreenW / iW_);
                } else {
                    iH = minScreenH;
                    iW = iW_ * (minScreenH / iH_);
                }
            }
            if (iW_ > maxScreenW || iH_ > maxScreenH) {
                if (iW_ / maxScreenW > iH_ / maxScreenH) {
                    iW = maxScreenW;
                    iH = iH_ * (maxScreenW / iW_);
                } else {
                    iH = maxScreenH;
                    iW = iW_ * (maxScreenH / iH_);
                }
            }

            let screenW = Math.max(minScreenW, Math.min(maxScreenW, iW));
            let screenH = Math.max(minScreenH, Math.min(maxScreenH, iH));

            if (screenW > iW && screenH < iH) {
                if (screenW > minScreenW) {
                    sX = (screenW / iW);
                    sY = 1;
                } else {
                }
            } else {
                if (screenH > iH && screenW < iW) {
                    if (screenH > minScreenH) {
                        sY = (screenH / iH);
                        sX = 1;
                    } else {
                    }
                } else {
                    let aspect = iW / iH;
                    let normal = screenW / screenH;
                    if (aspect > normal) {
                        sX = (normal / aspect);
                        sY = 1;
                    } else {
                        sY = (aspect / normal);
                        sX = 1;
                    }
                }
            }

            $(this.parentHTML).css("height", iH_ * sY + "px");
            $(this.parentHTML).css("width", iW_ * sX + "px");

            this.app.renderer.resize(screenW, screenH);

            $(this.parentHTML).css("margin-left", (iW_ - iW_ * sX) * 0.5 + "px");
            $(this.parentHTML).css("margin-top", (iH_ - iH_ * sY) * 0.5 + "px");

            this.contentContainer.x = screenW * 0.5;
            this.contentContainer.y = screenH * 0.5;

            this.back.clear();
            this.back.beginFill(0xFFFFFF, 1);
            this.back.drawRect(0, 0, screenW, screenH);
            this.back.endFill();
        }
    }

}
