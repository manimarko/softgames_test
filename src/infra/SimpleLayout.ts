import {Container} from "pixi.js";

export enum LayoutType {
    Vertical,
    Horizontal
}

export class SimpleLayout extends Container {
    type: LayoutType;
    delta: number = 0;

    constructor(type: LayoutType, delta) {
        super();
        this.type = type;
        this.delta = delta;
    }

    // @ts-ignore
    protected onChildrenChange(...args: any[]): void {
        super.onChildrenChange(args);
        this.updateChildren();
    }

    updateChildren() {
        let pos = 0;
        let keyPos = this.type == LayoutType.Horizontal ? "x" : "y";
        let keySize = this.type == LayoutType.Horizontal ? "width" : "height";
        for (let i = 0; i < this.children.length; i++) {
            this.children[i][keyPos] = pos;
            pos += this.children[i][keySize] + this.delta;
        }
    }
}