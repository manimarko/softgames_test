import {GameSelector} from "./app/GameSelector";
import {GameOne} from "./app/games/GameOne";
import {GameTwo} from "./app/games/GameTwo";
import {GameThree} from "./app/games/GameThree";
import {BaseContext} from "./infra/BaseContext";

let context = new BaseContext();

context.init(document.body, [1280, 1280], [720, 1280]);

let gameSelector = new GameSelector(context);
gameSelector.addGame(GameOne, "Game One");
gameSelector.addGame(GameTwo, "Game Two");
gameSelector.addGame(GameThree, "Game Tree");

context.contentContainer.addChild(gameSelector.mainContainer);

context.resize();
