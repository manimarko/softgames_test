import Container = PIXI.Container;
import Graphics = PIXI.Graphics;
import Point = PIXI.Point;
import {LayoutType, SimpleLayout} from "../infra/SimpleLayout";
import {UIContainer} from "../infra/UIContainer";
import {BaseGame} from "./BaseGame";
import {BaseContext} from "../infra/BaseContext";

export class GameSelector {
    static basicTextStyle = new PIXI.TextStyle({
        fontFamily: 'Ubuntu',
        fontSize: 30,
        fill: '#ffffff',
        align: 'center'
    });
    private gameClasses = {};
    private menuView: MenuView = new MenuView();
    private game: BaseGame;
    mainContainer: UIContainer = new UIContainer();
    contentContainer: UIContainer = new UIContainer();
    context: BaseContext;
    meter: FpsMeter = new FpsMeter();
    backLabel: BackLabel = new BackLabel();

    constructor(context: BaseContext) {
        this.mainContainer.addChild(this.contentContainer);
        this.mainContainer.addChild(this.meter);
        this.mainContainer.addChild(this.backLabel);
        this.context = context;
        context.app.ticker.add((dt: number) => {
            this.update(dt*0.016);
        });

        context.back.addListener("click", () => {
            this.onExitGame();
        });
        context.back.addListener("tap", () => {
            this.onExitGame();
        });

        this.setInitialState();
    }

    update(dt: number): any {
        if (this.game) {
            this.game.update(dt);
        }
        this.meter.update();
    }

    addGame(gameClass: Object, name: string) {
        this.gameClasses[name] = gameClass;
        this.menuView.addButton(name, () => {
            this.executeGame(name);
        })
    }

    executeGame(name) {
        this.contentContainer.removeChildren();
        let c = this.gameClasses[name];

        this.context.back.interactive = true;

        this.game = new c();
        this.contentContainer.addChild(this.game);
        this.game.init();
        this.game.start();

        this.backLabel.visible = true;
    }

    setInitialState() {
        this.contentContainer.addChild(this.menuView);
        this.context.back.interactive = false;
        this.backLabel.visible = false;
    }

    onExitGame() {
        this.contentContainer.removeChildren();
        this.game.stop();
        this.game.dispose();
        this.game.destroy(true);
        this.game = null;
        this.setInitialState();
        this.context.app.renderer.plugins.sprite.sprites.length = 0;
    }
}

export class FpsMeter extends UIContainer {
    private fps = 0;
    prevTime: number = 0;
    currTime: number = 0;
    tf: PIXI.Text;

    constructor() {
        super();
        this.tf = new PIXI.Text(name, GameSelector.basicTextStyle);
        this.anchorX = "c";
        this.anchorY = "t";
        this.yPos = 30;
        this.useParentSize = false;
        this.addChild(this.tf);
    }

    update() {
        let time = performance.now();
        this.currTime += time - this.prevTime;
        this.fps++;
        if (this.currTime >= 1000) {
            this.tf.text = "fps:" + this.fps;
            this.fps = 0;
            this.currTime = 0;
        }
        this.prevTime = time;
    }
}

export class BackLabel extends UIContainer {
    tf: PIXI.Text;

    constructor() {
        super();

        this.tf = new PIXI.Text("Tap Anywhere to back", GameSelector.basicTextStyle);
        this.anchorX = "c";
        this.anchorY = "b";
        this.yPos = 30;
        this.useParentSize = false;
        this.addChild(this.tf);
    }
}

export class MenuView extends UIContainer {

    static getSize(fillArea: number, aspect: number): Point {
        fillArea /= 100;
        let square = 1280 * 720 * fillArea;
        let y = Math.sqrt(square / aspect);
        let x = square / y;
        return new Point(x, y);
    }

    static createButton(name: string, fillArea: number, onClick: any): PIXI.Container {
        let btnCont = new Container();
        let bg = new Graphics();
        let size = MenuView.getSize(fillArea, 3);
        bg.lineStyle(2, 0xFFFFFF, 1);
        bg.beginFill(0xFF6633, 1);
        bg.drawRect(0, 0, size.x, size.y);
        bg.endFill();

        let tf = new PIXI.Text(name, GameSelector.basicTextStyle);
        tf.scale.x = Math.min(1, size.x / tf.width);
        tf.x = Math.round((size.x - tf.width) * 0.5);
        tf.y = Math.round((size.y - tf.height) * 0.5);
        tf.alpha = 0.5;
        btnCont.addChild(bg);
        btnCont.addChild(tf);
        btnCont.interactive = true;
        btnCont.addListener("tap", onClick);
        btnCont.addListener("click", onClick);
        return btnCont;
    }

    layout: SimpleLayout = new SimpleLayout(LayoutType.Vertical, 10);

    constructor() {
        super();
        this.addChild(this.layout);
        this.anchorX = this.anchorY = "c";
        this.useParentSize = false;
    }

    addButton(name, onClick) {
        this.layout.addChild(MenuView.createButton(name, 5, onClick));
    }
}


