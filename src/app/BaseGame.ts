import {UIContainer} from "../infra/UIContainer";

export class BaseGame extends UIContainer {

    constructor() {
        super();
        this.anchorX = this.anchorY = "c";
        this.useParentSize = false;
    }

    init(){

    }

    start(){

    }

    update(dt: number) {

    }

    stop(){

    }

    dispose(){

    }
}