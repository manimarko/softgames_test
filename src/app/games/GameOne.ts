import {BaseGame} from "../BaseGame";
import Texture = PIXI.Texture;
import Graphics = PIXI.Graphics;
import Sprite = PIXI.Sprite;

export class GameOne extends BaseGame {
    private texture: Texture;
    private beginAngle = 0;
    private deltaAngle = 0.1;

    constructor() {
        super();
    }

    init() {
        super.init();
        let bg = new Graphics();
        bg.lineStyle(2, 0xFFFFFF, 1);
        bg.beginFill(0xFF6633, 1);
        bg.drawRect(0, 0, 128, 128);
        bg.endFill();
        this.texture = bg.generateCanvasTexture();
        bg.destroy();
        this.createSprites();
        this.updateSpritesPosition();
    }

    createSprites() {
        for (let i = 0; i < 144; i++) {
            let sprite = new Sprite(this.texture);
            this.addChild(sprite);
        }
    }

    updateSpritesPosition() {
        for (let i = 0; i < this.children.length; i++) {
            let child = this.children[i];

            //we save 144*2 function call
            // @ts-ignore
            child.transform.position._x = i * 5;
            // @ts-ignore
            child.transform.position._y = Math.sin(this.beginAngle + i * this.deltaAngle) * 100 + 50;
            // @ts-ignore
            child.transform._localID++;
        }
    }

    update(dt: number) {
        super.update(dt);
        this.updateSpritesPosition();
        this.beginAngle += this.deltaAngle * dt * 10;
    }

    dispose() {
        this.texture.destroy(true);
        this.texture = null;
        super.dispose();
    }
}