import {BaseGame} from "../BaseGame";
import {Texture} from "pixi.js";
import {LayoutType, SimpleLayout} from "../../infra/SimpleLayout";
import {GameSelector} from "../GameSelector";
import Graphics = PIXI.Graphics;
import Sprite = PIXI.Sprite;
import DisplayObject = PIXI.DisplayObject;

export class GameTwo extends BaseGame {
    private texture: Texture;
    private layout: SimpleLayout = new SimpleLayout(LayoutType.Horizontal, 20);
    private time = 0;
    tf: PIXI.Text;

    constructor() {
        super();
    }

    init() {
        super.init();
        let bg = new Graphics();
        bg.lineStyle(2, 0xFFFFFF, 1);
        bg.beginFill(0xFF6633, 1);
        bg.drawRect(0, 0, 128, 128);
        bg.endFill();
        this.texture = bg.generateCanvasTexture();

        this.tf = new PIXI.Text(name, new PIXI.TextStyle({
            fontFamily: 'Ubuntu',
            fontSize: 30,
            fill: '#ffffff',
            align: 'left'
        }));
        this.tf.y = -100;
        this.addChild(this.tf);
        this.addChild(this.layout);
    }

    update(dt: number) {
        super.update(dt);
        this.time += dt;
        this.tf.text = "wait for next generation:" + (2 - this.time).toFixed(2);
        if (this.time > 2) {
            this.time = 0;
            this.nextSet();
        }
    }

    nextSet() {
        let set = ["icon", "text"];
        let count = Math.random() * 4 + 3;
        let itemsArray = [];
        for (let i = 0; i < count; i++) {
            itemsArray.push(set[Math.floor(set.length * Math.random())]);
        }
        this.createSet(itemsArray);
    }

    createSet(arr: Array<string>) {
        for (let i = 0; i < this.layout.children.length; i++) {
            this.layout.children[i].destroy();
        }
        this.layout.removeChildren();
        for (let i = 0; i < arr.length; i++) {
            this.layout.addChild(this.createObject(arr[i]));
        }
    }

    createObject(type: string): DisplayObject {
        if (type == "icon") {
            return new Sprite(this.texture);
        }
        if (type == "text") {
            return new PIXI.Text("SampleText", GameSelector.basicTextStyle);
        }
    }

    dispose() {
        this.texture.destroy(true);
        this.tf.destroy(true);
        super.dispose();
    }
}