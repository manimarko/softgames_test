import {BaseGame} from "../BaseGame";

import Graphics = PIXI.Graphics;
import Texture = PIXI.Texture;


export class GameThree extends BaseGame {
    private texture: Texture;
    private emitter:any;

    update(dt: number) {
        super.update(dt);
        this.emitter.update(dt);
        this.enabledAutoSetPos = false;
    }

    init(){
        let bg = new Graphics();
        bg.beginFill(0xFFFFFFF, 1);
        bg.drawCircle(0, 0, 100);
        bg.endFill();
        this.texture = bg.generateCanvasTexture();

        // @ts-ignore
        this.emitter = new window.particles.Emitter(

            // The PIXI.Container to put the emitter in
            // if using blend modes, it's important to put this
            // on top of a bitmap, and not use the root stage Container
            this,

            // The collection of particle images to use
            [this.texture],

            // Emitter configuration, edit this to change the look
            // of the emitter
            {
                alpha: {
                    list: [
                        {
                            value: 0.0,
                            time: 0
                        },
                        {
                            value: 1,
                            time: 0.2
                        },
                        {
                            value: 0.0,
                            time: 1
                        }
                    ],
                    isStepped: false
                },
                "scale": {
                    list: [
                        {
                            value: 0.2,
                            time: 0
                        },
                        {
                            value: 0.4,
                            time: 0.2
                        },
                        {
                            value: 1,
                            time: 1
                        }
                    ],
                    "minimumScaleMultiplier": 1
                },
                color: {
                    list: [
                        {
                            value: "#ffbb00",
                            time: 0
                        },
                        {
                            value: "#ffbb00",
                            time: 0.1
                        },
                        {
                            value: "#ff3300",
                            time: 0.4
                        },
                        {
                            value: "#010101",
                            time: 1
                        }
                    ],
                    isStepped: false
                },
                "speed": {
                    "start": 2,
                    "end": 1,
                    "minimumSpeedMultiplier": 1
                },
                "acceleration": {
                    "x": 0,
                    "y": -100
                },
                "maxSpeed": 0,
                "startRotation": {
                    "min": 0,
                    "max": 360
                },
                "noRotation": true,
                "rotationSpeed": {
                    "min": 0,
                    "max": 0
                },
                "lifetime": {
                    "min": 2.0,
                    "max": 3
                },
                "blendMode": "add",
                "frequency": 0.3,
                "emitterLifetime": -1,
                "maxParticles": 10,
                "pos": {
                    "x": 0,
                    "y": 0
                },
                "addAtBack": false,
                "spawnType": "point"
            }
        );
    }

    constructor() {
        super();
    }

    dispose() {
        this.emitter.destroy();
        this.texture.destroy(true);
        this.texture = null;
        super.dispose();
    }
}