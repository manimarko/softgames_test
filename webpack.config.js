const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const distPath = path.join(__dirname, '/dist');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: "./src/index.ts",

    optimization: {
        removeAvailableModules: false,
        removeEmptyChunks: false,
        splitChunks: false,
    },

    output: {
        pathinfo: false
    },

    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: [{
                    loader: 'ts-loader',
                    options: {
                        transpileOnly: true,
                        experimentalWatchApi: true,
                    },
                }],
            }
        ]
    },

    resolve: {
        extensions: [ '.ts', '.tsx', '.js', '.d.ts' ]
    },

    plugins: [
        new ForkTsCheckerWebpackPlugin(),
        new CopyWebpackPlugin([
            { from: 'node_modules/pixi.js/dist/pixi.min.js', to: '' },
            { from: 'node_modules/jquery/dist/jquery.js', to: '' },
            { from: 'node_modules/pixi-particles/dist/pixi-particles.js', to: '' },
        ], {}),
        new HtmlWebpackPlugin({
            template: './src/template.html'
        })
    ],

    devServer: {
        contentBase: distPath,
        port: 9000,
        compress: true,
        open: true
    }
};